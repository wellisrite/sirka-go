# Sirka - GO

Study Case for Sirka application backend

  - Need to [remove the volume](https://stackoverflow.com/questions/56657683/postgres-docker-image-is-not-creating-database-with-custom-name) by `docker volume ls` and `docker volume rm <uuid>`
  - The Gorm seem faulty, need to add the key words: `sslmode:disable`, [see more info](https://www.calhoun.io/connecting-to-a-postgresql-database-with-gos-database-sql-package/)
  - Great help from [here](https://www.reddit.com/r/docker/comments/8szjw0/how_to_connect_to_postgresql_using_dockercompose/)
  - Start project
    - go mod init
    - go mod vendor
    - go run main.go
  - **Deploy into Kubernetes**
    - [Kubernetes-container-service-GitLab-sample on IBM](https://github.com/IBM/Kubernetes-container-service-GitLab-sample/blob/master/kubernetes/postgres.yaml)
    - [golang-example.yaml](./golang-example-deployment.yaml)
    - [postgret-deployment.yaml](./postgret-deployment.yaml)
  - [Using go mod download to speed up Golang Docker builds](https://medium.com/@petomalina/using-go-mod-download-to-speed-up-golang-docker-builds-707591336888)
  - **Using Power environment**
    - `docker network create golang-example`
    - `docker run -d -e POSTGRES_DB=postgres -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=example --name postgres --network golang-example --mount source=prosgret-vol,destination=/var/lib/postgresql/data -p 5432:5432 postgres:latest`
    - `docker run -d -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=example -e POSTGRES_DB=postgres -e POSTGRES_HOST=postgres -e POSTGRES_PORT=5432 --network golang-example --name golang-example -p 8080:8080 moxing9876/golang-example:0.0.3`
      - example output
      ```
      CONTAINER ID        IMAGE                             COMMAND                  CREATED             STATUS              PORTS                    NAMES
      9168b3afde13        moxing9876/golang-example:0.0.3   "/go/bin/hello"          4 seconds ago       Up 3 seconds        0.0.0.0:8080->8080/tcp   golang-example
      6949a79586fb        postgres:latest                   "docker-entrypoint.s…"   6 minutes ago       Up 6 minutes        0.0.0.0:5432->5432/tcp   postgres
      ```
    - `$ curl http://localhost:8080/displayallusers`
      - example output : `[
        {
            "id": 1,
            "name": "budi"
        }
      ]`
