package controllers

import (
	models "golang-example/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
)

// GET /Users
// Get all Users
func FindUsers(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	var Users []models.User
	db.Find(&Users)

	c.JSON(http.StatusOK, Users)
}

// POST /Users
// Find a User
func FindUser(c *gin.Context) {
	db := c.MustGet("db").(*gorm.DB)

	// Get model if exist
	var User models.User
	if err := db.Where("id = ?", c.PostForm("id")).First(&User).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	c.JSON(http.StatusOK, User)
}
