package main

import (
	controllers "golang-example/controllers" // new
	"golang-example/models"                  // new

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	db := models.SetupModels() // new

	// Provide db variable to controllers
	r.Use(func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	})

	r.GET("/sirka/displayallusers", controllers.FindUsers)

	r.POST("/sirka/displayuser", controllers.FindUser) // find by id

	r.Run()
}
